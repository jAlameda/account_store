<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Users',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
	    $form = ActiveForm::begin();
		    echo $form->field($model, 'firstname');
		    echo $form->field($model, 'lastname');
		    echo $form->field($model, 'role_id')->dropDownList($roles)->label('Role');
		    echo $form->field($model, 'email')->input('email');
		    echo $form->field($model, 'username')->label('Username');
		    echo $form->field($model, 'password')->input('password')->label('Password');
		    //echo $form->field($model, 'password')->input('password')->label('Confirm your password');
		?>
		    <div class="form-group">
		    	<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
		    </div>
		<?php
	    ActiveForm::end();
    ?>

</div>
