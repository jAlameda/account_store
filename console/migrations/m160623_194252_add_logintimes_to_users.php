<?php

use yii\db\Migration;

/**
 * Handles adding logintime to table `users`.
 */
class m160623_194252_add_logintimes_to_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'login_time', $this->datetime());
        $this->addColumn('users', 'logout_time', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'login_time');
        $this->dropColumn('users', 'logout_time');
    }
}
