<?php

use yii\db\Migration;

class m160620_214407_add_user_authKey_column extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'auth_key', 'varchar(32) AFTER `password`');
    }

    public function down()
    {
        $this->dropColumn('users', 'auth_key');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
