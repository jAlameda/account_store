<?php

use yii\db\Migration;

/**
 * Handles dropping users_id from table `roles`.
 */
class m160621_202834_drop_users_id_from_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('roles', 'users_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('roles', 'users_id', $this->int(10));
    }
}
